# coding: utf-8

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from configparser import ConfigParser

driver = WebDriver()
cp = ConfigParser()
cp.read('config.ini')

stand_address = cp.get('stand', 'address')
lgn = cp.get('root', 'login')
pswd = cp.get('root', 'password')
login_xpath = './/input[@id="loginEdit-el"]'
password_xpath = './/input[@id="passwordEdit-el"]'
timeout = 15

driver.get(stand_address)

def save_button(driver: WebDriver) -> WebElement:
    save_button_xpath = './/span[@data-item-marker="SaveButton"]'
    save_button_element = driver.find_element_by_xpath(save_button_xpath)
    return save_button_element

def login(login: str, password: str, driver: WebDriver):
    def login_button() -> WebElement:
        login_button_xpath = './/span[@data-item-marker="btnLogin"]'
        login_button = driver.find_element_by_xpath(login_button_xpath)
        return login_button

    login_input = WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((By.XPATH, login_xpath)))

    # why text returns '' after logout?
    # if login_input.text:
    #     login_input.clear()
    login_input.clear()
    login_input.send_keys(login)

    password_input = driver.find_element_by_xpath(password_xpath)
    password_input.send_keys(password)

    login_button().click()

def exit_from_system():
    user_profile_button_xpath = './/span[@id="profile-user-button-wrapperEl"]'
    user_profile_button_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, user_profile_button_xpath)))
    user_profile_button_element.click()

    exit_profile_button_xpath = './/li[@data-item-marker="Выход"]'
    exit_profile_button_element = driver.find_element_by_xpath(exit_profile_button_xpath)
    exit_profile_button_element.click()

def move_to_system_designer_page():
    system_designer_xpath = './/a[@data-item-marker="Дизайнер системы"]'
    system_designer_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, system_designer_xpath)))
    system_designer_element.click()

def move_to_users_page():
    users_xpath = './/a[@data-item-marker="Пользователи системы"]'
    users_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, users_xpath)))
    users_element.click()

def fill_login_and_pass_for_user(login: str, password: str):
    login_input_xpath = './/input[@id="UserPageV2NameTextEdit-el"]'
    password_input_xpath = './/input[@id="new-password-el"]'
    password_confirm_input_xpath = './/input[@id="new-password-confirmation-el"]'
    save_button_xpath = './/span[@data-item-marker="SaveButton"]'

    login_input_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, login_input_xpath)))
    login_input_element.send_keys(login)

    password_input_element = driver.find_element_by_xpath(password_input_xpath)
    password_input_element.send_keys(password)
    password_confirm_input_element = driver.find_element_by_xpath(password_confirm_input_xpath)
    password_confirm_input_element.send_keys(password)

def create_user(fio):
    ####
    add_user_button_xpath = './/span[@data-item-marker="SeparateModeAddOnlyEmployeeButton"]'
    add_user_button_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, add_user_button_xpath)))
    add_user_button_element.click()
    ####


    ####
    # клик по лупе
    find_contact_xpath = './/div[@id="UserPageV2ContactLookupEdit-right-icon"]'
    contact_lookup_edit_id = '"UserPageV2ContactLookupEdit-right-icon"'
    find_contact_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, find_contact_xpath)))
    driver.execute_script('document.getElementById(' + contact_lookup_edit_id + ').click();')
    ####


    ####
    # выбор "добавить" из меню
    add_contact_button_xpath = './/span[@data-tag="add"]'
    add_contact_button_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, add_contact_button_xpath)))
    add_contact_button_element.click()
    ####


    ####
    fio_input_xpath = './/input[@id="ContactPageV2AccountNameTextEdit-el"]'
    fio_input_element = WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((By.XPATH, fio_input_xpath)))
    fio_input_element.send_keys(fio)

    save_button(driver=driver).click()
    ####


######################
login(lgn, pswd, driver)
move_to_system_designer_page()
move_to_users_page()
create_user("Apachai Hopachai")

# saving login and password for created user
new_login = "Apachai"
new_password = "apahopa"
fill_login_and_pass_for_user(new_login, new_password)
save_button(driver).click()

exit_from_system()

login(new_login, new_password, driver)
exit_from_system()

driver.quit()
